package com.sdg.metatesting.model

class MailingAddressTest extends AbstractModelTest {

    def "test equals and hashCode"() {
        expect:
        testEqualsAndHashCode(MailingAddress.class)
    }

    def "test toString"() {
        expect:
        toString()
    }
}
