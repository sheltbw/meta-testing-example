package com.sdg.metatesting.model

class PersonTest extends AbstractModelTest{
    def "test equals and hashCode"() {
        expect:
        testEqualsAndHashCode(Person.class)
    }

    def "test toString"() {
        expect:
        testToString(Person.class)
    }
}
